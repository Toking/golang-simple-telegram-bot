package bot

import (
	"fmt"
	"time"
)

func generateUserSequential() (string, string)  {
	start := time.Now()

	name := getRandomName()
	pic := getRandomPictureUrl()

	after := time.Since(start)
	fmt.Printf("time used %s \n", after)

	return name, pic
}

func generateUserInParallel() (string, string)  {
	start := time.Now()

	nameChan := make(chan string)
	picChan := make(chan string)

	go func() {
		name := getRandomName()
		nameChan <- name
	}()

	go func() {
		pic := getRandomPictureUrl()
		picChan <- pic
	}()

	name := <- nameChan
	pic := <- picChan

	after := time.Since(start)
	fmt.Printf("time used %s \n", after)

	return name, pic
}
