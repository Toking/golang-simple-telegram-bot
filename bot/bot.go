package bot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
	"strings"
)

func Start(token string)  {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}
		message := tgbotapi.Message{}
		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		switch strings.ToLower(update.Message.Text) {

		case "man":
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "You are man!")

			bot.Send(msg)

		case "woman":
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "You are woman!")

			bot.Send(msg)

		case "seq":
			name, url := generateUserSequential()
			log.Printf("Url " + url)
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, name)
			image := tgbotapi.NewPhotoShare(update.Message.Chat.ID, url)

			bot.Send(msg)
			bot.Send(image)

		case "par":
			name, url := generateUserInParallel()
			log.Printf("Url " + url)
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, name)
			image := tgbotapi.NewPhotoShare(update.Message.Chat.ID, url)

			bot.Send(msg)
			bot.Send(image)
		case "choice":
			markup := sendKeyboard()

			message := tgbotapi.NewMessage(update.Message.Chat.ID, "Choose your sex:")
			message.ReplyMarkup = markup

			bot.Send(message)

		default:
			message = tgbotapi.Message{
				Text: "hello",
			}
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, message.Text)
			bot.Send(msg)
		}
	}
}

func sendKeyboard() interface{} {
	var markup interface{}
	keys := []tgbotapi.KeyboardButton{}
	keys = append(keys, tgbotapi.NewKeyboardButton("Man"))
	keys = append(keys, tgbotapi.NewKeyboardButton("Woman"))

	keyboard := tgbotapi.NewReplyKeyboard(keys)
	keyboard.OneTimeKeyboard = true

	markup = keyboard
	return markup
}
