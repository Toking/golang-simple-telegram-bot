package bot

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type NameResponse struct {
	Name    string `json:"name"`
	Surname string `json:"surname"`
	Gender  string `json:"gender"`
	Region  string `json:"region"`
}

func getRandomName() string  {
	log.Println("get random name")
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://uinames.com/api/", nil)
	if err != nil {
		log.Fatal(err)
		return ""
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return ""
	}
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Fatal(err)
		return ""
	}

	var responseObj NameResponse
	err = json.Unmarshal(body, &responseObj)
	if err != nil {
		log.Fatal(err)
		return ""
	}

	return fmt.Sprintf("%s %s", responseObj.Name, responseObj.Surname)

}
