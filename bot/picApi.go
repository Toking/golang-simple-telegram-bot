package bot

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type Picture struct {
	Large     string `json:"large"`
	Medium    string `json:"medium"`
	Thumbnail string `json:"thumbnail"`
}

type PictureResponse struct {
	Results []Result `json:"results"`
}

type Result struct {
	Picture Picture `json:"picture"`
}

func getRandomPictureUrl() string {
	log.Println("get random picture url")
	client := http.Client{}
	req, err := http.NewRequest("GET", "https://randomuser.me/api/", nil)

	if err != nil {
		fmt.Println(err)
		return ""
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		fmt.Println(err)
		return ""
	}

	var responseObj PictureResponse
	err = json.Unmarshal(body, &responseObj)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	return responseObj.Results[0].Picture.Large

}