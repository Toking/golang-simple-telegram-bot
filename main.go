package main

import (
	"fmt"
	"github.com/tom170896/telegram-bot/bot"
	"github.com/tom170896/telegram-bot/config"
)

func main() {
	c, err := config.Init()
	if err != nil {
		fmt.Println(err)
		return
	}
	bot.Start(c.Token)
}